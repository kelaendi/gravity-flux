using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    private const int levelAmount = 2;
    private void OnTriggerEnter2D(Collider2D collision){
        if(collision.transform.tag == "Player"){
            AudioManager.Instance.PlayOneShot(FmodEvents.Instance.WinSound, this.transform.position);
            int currentIndex = SceneManager.GetActiveScene().buildIndex;
            if(currentIndex < levelAmount){
                SceneManager.LoadSceneAsync(currentIndex+1);
            } else {
                SceneManager.LoadSceneAsync(0);
            }
        }
    }
}
