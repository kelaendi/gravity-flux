using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Transform currentCheckpoint; // stores the position of the current checkpoint or spawning

    private Player1 player;
    private bool isActive;

    //Activate the checkpoint
    private void OnTriggerEnter2D(Collider2D collision){
        if(collision.transform.tag == "Player"){
            AudioManager.Instance.PlayOneShot(FmodEvents.Instance.CheckpointSound, this.transform.position);
            currentCheckpoint = transform; //store position of latest checkpoint activated
            collision.GetComponent<Health>().spawn = currentCheckpoint.position;
            GetComponent<Collider2D>().enabled = false;
        }
    }
}
