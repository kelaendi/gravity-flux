using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class gravity : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float timer;
    [SerializeField] private float gravCooldown;
    [SerializeField] private float cooldownTimer = Mathf.Infinity;
    [SerializeField] private Player1 player;
    private bool goDowncheck = false;

    // Update is called once per frame
    void Update()
    {
        GraviticFlux();

        if(timer <= 0){
            rb.gravityScale = math.abs(rb.gravityScale);
            goDowncheck = false;
        }
        else {
            timer -= Time.deltaTime;
        }
        
        cooldownTimer += Time.deltaTime;
    }

    void GraviticFlux()
    {
        // if (Input.GetButtonDown("Vertical") && cooldownTimer > gravCooldown)
        if (Input.GetKeyDown(KeyCode.UpArrow) && cooldownTimer > gravCooldown && !goDowncheck)
        {               
            cooldownTimer = 1.5f; //was 0, this way the cool down is 
            goDowncheck = true;
            if (rb != null)
            {
                AudioManager.Instance.PlayOneShot(FmodEvents.Instance.GravityShiftSound, this.transform.position);
                timer = 2f;
                rb.gravityScale *= -1f;
                UnityEngine.Debug.Log("Gravity Scale: " + rb.gravityScale);
            }
            else
            {
                UnityEngine.Debug.LogWarning("Rigidbody not assigned.");
            }
        }
        if(Input.GetKeyDown(KeyCode.DownArrow) && goDowncheck){
                AudioManager.Instance.PlayOneShot(FmodEvents.Instance.GravityShiftSound, this.transform.position);
                goDowncheck = false;
                rb.gravityScale *= -1f;
                UnityEngine.Debug.Log("Gravity Scale: " + rb.gravityScale);
            }  
    }
}
