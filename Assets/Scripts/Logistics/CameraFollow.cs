using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Rigidbody2D rb;
    public float offsetX = 6f;
    public float offsetY = 0f;
    void Update()
    {
        var position = transform.position;
        position.x = target.transform.position.x + offsetX;
        position.y = target.transform.position.y + offsetY;
        transform.position = position; 
    }

    // void offsetManager(height){
    //     if(rb.position.y -  >=)
    // }
}
