using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play(){
        SceneManager.LoadSceneAsync(1);

    }

    public void ChooseLevel(){
        // Open Level panel i.e. setActive
        SceneManager.LoadSceneAsync("LevelMenu");
    }

    public void Settings(){
        // Open options panel i.e. setActive
    }

    public void Exit(){
        Application.Quit();
        
    }
}
