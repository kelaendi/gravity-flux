using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// public class spiderEnemy : MonoBehaviour
// {
//     [SerializeField] private float attackCooldown;

//     [SerializeField] private float range;
//     [SerializeField] private float colliderDistance;

//     [SerializeField] private float damage;

//     [SerializeField] private BoxCollider2D boxCollider;
//     [SerializeField] private LayerMask playerLayer;
//     [SerializeField] private Health playerHealth;

//     private float cooldownTimer = Mathf.Infinity;
//     private enemyPatrol _enemyPatrol;

//     private void Awake(){
//         _enemyPatrol = GetComponentInParent<enemyPatrol>();
//     }
//     private void Update(){

//         if(PlayerDetected())
//              cooldownTimer += Time.deltaTime;
//              if(cooldownTimer > attackCooldown){
//                  AttackPlayer();
//                  cooldownTimer = 0;
//              }
//         if(_enemyPatrol != null)
//             _enemyPatrol.enabled = !PlayerDetected();
//     }
//     private bool PlayerDetected(){
        
//         RaycastHit2D hit = Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.x * colliderDistance, 
//         new Vector3(boxCollider.bounds.size.x, boxCollider.bounds.size.y + 0.5f, boxCollider.bounds.size.z), 0, Vector2.left, 0, playerLayer);
        
//         if(hit.collider != null){
//             playerHealth = hit.transform.GetComponent<Health>();    
//         } 
//         return hit.collider != null;
        
//     }
//     private void AttackPlayer(){
//         if(PlayerDetected()){
//             playerHealth.TakeDamage(damage);
//         }
//     }
// }
