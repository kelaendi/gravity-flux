using System;
using System.Data.Common;
using UnityEngine;

public class enemyPatrol : MonoBehaviour
{
    [Header ("Patrol Points")]
    [SerializeField] private Transform leftEdge;
    [SerializeField] private Transform rightEdge;

    [Header ("Enemy")]
    [SerializeField] private Transform enemy;

    [Header ("Movement Parameters")]
    [SerializeField] private float speed;
    private Vector3 initScale;
    [SerializeField] private bool movingLeft;
    [SerializeField] private float idleDuration;
    private float idleTimer;

    private void Awake(){
        initScale = enemy.localScale;
    }

    private void Update(){
        if(!transform.CompareTag("Player")){
            if(movingLeft){
                if(enemy.position.x >= leftEdge.position.x)
                    MoveInDirection(-1);
                else 
                    DirectChange();
            }
            else {
                if(enemy.position.x <= rightEdge.position.x)
                    MoveInDirection(1);
                else 
                    DirectChange();
            }
        }
    }

    private void DirectChange(){

        idleTimer += Time.deltaTime;
        if(idleTimer > idleDuration)
            movingLeft = !movingLeft;
    }
    private void MoveInDirection(int _direction){
        idleTimer = 0;
        // make enemy face direction
        //enemy.localScale = new Vector3(Math.Abs(initScale.x) * _direction, initScale.y, initScale.z); 
        //move in direction
        enemy.position = new Vector3(enemy.position.x + Time.deltaTime * _direction * speed, enemy.position.y, enemy.position.z);
    }
}
