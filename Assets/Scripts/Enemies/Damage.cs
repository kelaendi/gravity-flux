using Unity.VisualScripting;
using UnityEngine;

public class Damage : MonoBehaviour
{
    
    [SerializeField] private float damage;
    void Start()
    {
        GameObject myGameObject = this.gameObject;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        switch(gameObject.tag){
            // for shooting
            case "Enemy":
                if (collision.tag == "Player")
                {
                 collision.GetComponent<Health>().TakeDamage(damage);
                }
                break;

            // for enemy AI and traps
            case "Player":
                if (collision.tag == "Enemy")
                {
                 collision.GetComponent<Health>().TakeDamage(damage);
                }
                break;
            
            default:
                break;
        }

            
    }
}
