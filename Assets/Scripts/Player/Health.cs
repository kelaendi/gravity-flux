using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;




public class Health : MonoBehaviour
{
    [SerializeField] public float startingHealth;
    [SerializeField] public Vector2 spawn;

    private float currentHealth;
    [SerializeField] public Player1 player;

    // public spiderEnemy spider;
    public bool dead = false;

    // Animator
    public Animator animator;


    private void Awake()
    {
        spawn = new Vector3(0f, 0f, 0f);
        currentHealth = startingHealth;
    }
    public void TakeDamage(float _damage){
        currentHealth = Math.Clamp(currentHealth - _damage, 0, startingHealth);

        if (currentHealth > 0){
            // player hurt
        }
        else {
            AudioManager.Instance.PlayOneShot(FmodEvents.Instance.EnemySound, this.transform.position);
            animator.SetBool("dead", true);
            Freeze();
            Respawn();
        }
    }
    public void Respawn(){
        AudioManager.Instance.PlayOneShot(FmodEvents.Instance.RespawnSound, this.transform.position);
        player.GetComponent<Rigidbody2D>().position = spawn;
        currentHealth = startingHealth;
    }


    public void Freeze()
    {
        StartCoroutine(FreezeRoutine());
    }

    private IEnumerator FreezeRoutine()
    {
        dead = true;
        Time.timeScale = 0;

        float pauseEndTime = Time.realtimeSinceStartup + 2;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }

        Time.timeScale = 1;

        dead = false;
        animator.SetBool("dead", false);
        player.GetComponent<Rigidbody2D>().gravityScale = MathF.Abs( player.GetComponent<Rigidbody2D>().gravityScale);
    }
}
