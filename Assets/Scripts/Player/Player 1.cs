using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class Player1 : MonoBehaviour
{
    // moving
    private float horiz;
    public float speed = 25f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    private bool isFacingRight;

    // Jumping
    private bool jumpBuffer;
    [SerializeField] private float jumpPower;

    // coyote time
    private float coyote_timer;

    // Animator
    public Animator animator;
    
    // other
    public GameObject checkpoint;

    // Dash
    [SerializeField] private float dashTimer = Mathf.Infinity;
    [SerializeField] private float dashCooldown;
    void Awake(){
        isFacingRight = true;
    }
    void Update()
    {
        horiz = Input.GetAxisRaw("Horizontal");
        Flip();
        updateCoyoteTimer();
        Jump();
        Dash();
        dashTimer += Time.deltaTime;
    }

    private void Jump(){
        if(Input.GetButtonDown("Jump") && !IsGrounded()){
            jumpBuffer = true;
        }

        if ((jumpBuffer || Input.GetButtonDown("Jump")) && (IsGrounded() || coyote_timer < 0.1f))
        {
            rb.velocity = new Vector2(rb.velocity.x,rb.gravityScale * jumpPower / math.abs(rb.gravityScale));
            jumpBuffer = false;

            AudioManager.Instance.PlayOneShot(FmodEvents.Instance.JumpSound, this.transform.position);
        }
    }


    private void updateCoyoteTimer(){
        if(!IsGrounded()){
            coyote_timer += Time.deltaTime;
        }
        else {
            coyote_timer = 0f;
        }
    }
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horiz * speed, rb.velocity.y);

        animator.SetFloat("Speed", Mathf.Abs(rb.velocity.magnitude));
    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.5f, groundLayer);
    }
    private void Flip()
    {
        Vector3 localScale = transform.localScale;
        if ((isFacingRight && horiz < 0f) || (!isFacingRight && horiz > 0f))
        {
            isFacingRight = !isFacingRight;
            localScale.x *= -1f; 
            transform.localScale = localScale;
        }
        if (rb.gravityScale < 0 && localScale.y > 0 || rb.gravityScale > 0 && localScale.y < 0){
            localScale.y *= -1f;
            transform.localScale = localScale;
        }
    }
    

    private void Dash(){
        if(Input.GetKeyDown(KeyCode.M) && dashTimer > dashCooldown){
            dashTimer = 0f;
            rb.velocity = new Vector2(rb.gravityScale * jumpPower / math.abs(rb.gravityScale), rb.velocity.y);
        }
    }
}
