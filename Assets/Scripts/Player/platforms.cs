using UnityEngine;

public class platforms : MonoBehaviour
{
    private Transform originalParent;
    public bool isOnPlatform;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            UnityEngine.Debug.Log("collision, changing parents");
            // Store the original parent
            originalParent = transform.parent;
            // Set the player as a child of the platform
            transform.SetParent(collision.transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            // Restore the original parent
            transform.SetParent(originalParent);
        }
    }
}
