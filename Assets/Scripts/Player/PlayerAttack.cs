using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private Player1 player;
    [SerializeField] private float attackCooldown;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject[] ammo;

    [SerializeField] private float cooldownTimer = Mathf.Infinity;

    void Awake(){
        player = GetComponent<Player1>();
        Debug.Log("Player initialized: " + (player != null));
        foreach (var bullet in ammo)
        {
            Debug.Log("Bullet initialized: " + bullet.name);
        }
    }

    private void Update(){
        if(Input.GetKeyDown(KeyCode.F) && (cooldownTimer > attackCooldown))
            Attack();
        cooldownTimer += Time.deltaTime;
    }
 
    private void Attack(){
        cooldownTimer = 0;
        // we're gonna use object pooling for fireballs so as to not generate new ones every time
        int bulletIndex = FindBullet();
        GameObject bullet = ammo[bulletIndex];
        
        // Set the position and direction
        bullet.transform.position = firePoint.position;
        Vector3 bulletScale = bullet.transform.localScale; // Store original bullet scale
        float direction_ = Mathf.Sign(transform.localScale.x); // Use Mathf.Sign for more accuracy
        bullet.GetComponent<projectile>().SetDirection(direction_);
        if (direction_ < 0)
        {
            bulletScale.x *= -1f; // Flip bullet horizontally
            bullet.transform.localScale = bulletScale; // Apply the scale change
        }

    }

    private int FindBullet(){
        for(int i = 0; i < ammo.Length; i++){
            if(!ammo[i].activeInHierarchy){
                return i;
            }
        }
        return 0;
    }
}
