using System;
using UnityEngine;


public class projectile : MonoBehaviour
{
    [SerializeField] private float speed;
    private bool hit;
    private float direction;
    private BoxCollider2D boxCollider;
    private void Awake(){
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void Update(){
        if(hit) return;
        float movementSpeed = speed * Time.deltaTime * direction;
        transform.Translate(movementSpeed,0,0);
        Debug.Log("Projectile moving: Speed = " + movementSpeed + " Direction = " + direction + "Delta time = " + Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision){
        if (!collision.CompareTag("Player"))
        {
            hit = true;
            boxCollider.enabled = false;
            gameObject.SetActive(false);
            Debug.Log("Projectile hit: " + collision.name + " Projectile deactivated.");
        }
        if(collision.tag == "Enemy" && collision != null){
            collision.GetComponent<Health>().TakeDamage(1);
        }
    }

    public void SetDirection(float _direction){
        direction = _direction;
        gameObject.SetActive(true);
        hit = false;
        boxCollider.enabled = true;

        // Set the projectile to face the correct direction explicitly
        float localScaleX = Mathf.Abs(transform.localScale.x) * (_direction > 0 ? 1 : -1);
        transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }
}
