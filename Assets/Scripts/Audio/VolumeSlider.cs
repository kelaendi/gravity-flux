using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    private enum VolumeType {
        // MASTER,
        MUSIC,
        SOUNDS
    }
    [Header("Type")]
    [SerializeField] private VolumeType volumeType;

    private Slider volumeSlider;

    private void Awake(){
        volumeSlider = this.GetComponentInChildren<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        switch (volumeType){
            case VolumeType.MUSIC:
                volumeSlider.value = AudioManager.Instance.MusicVolume;
                break;

            case VolumeType.SOUNDS:
                volumeSlider.value = AudioManager.Instance.SoundsVolume;
                break;

            default:
                Debug.LogWarning("Not a valid volume type");
                break;
        }
    }

    public void OnSliderValueChanged(){
        switch (volumeType){
            case VolumeType.MUSIC:
                AudioManager.Instance.MusicVolume = volumeSlider.value;
                break;

            case VolumeType.SOUNDS:
                AudioManager.Instance.SoundsVolume = volumeSlider.value;
                break;

            default:
                Debug.LogWarning("Not a valid volume type");
                break;
        }

    }
}
