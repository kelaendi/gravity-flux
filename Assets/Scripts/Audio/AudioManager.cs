using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using FMOD.Studio;

public class AudioManager : MonoBehaviour
{
    [Header("Volume")]
    [Range(0,1)]

    // public float MasterVolume = 1f;
    public float MusicVolume = 0.7f;
    public float SoundsVolume = 0.7f;
    private Bus musicBus, soundsBus;
    public static AudioManager Instance { get; private set;}

    private void Awake(){ 
        //setup singleton
        if(Instance != null){
            Debug.LogError("More than one AudioManager in scene!");
        }
        Instance = this;

        musicBus = RuntimeManager.GetBus("bus:/Music");
        soundsBus = RuntimeManager.GetBus("bus:/Sounds");
    }

    public void PlayOneShot(EventReference sound, Vector3 worldPos) {
        RuntimeManager.PlayOneShot(sound, worldPos);
    }
    
    // public Slider MusicSlider, SoundEffectsSlider;

    void Update()
    {
        musicBus.setVolume(MusicVolume);
        soundsBus.setVolume(SoundsVolume);

    }

    void Start() { 
        // InitializeMusic(FmodEvents.Instance.Music);
    }
    // public void ToggleMuteEffects(){
    //     if (effectsMuted){
    //         ChangeSoundEffectsVolume(1);
    //         effectsMuted = false;
    //     } else {
    //         ChangeSoundEffectsVolume(0);
    //         effectsMuted = true;
    //     }        
    // // }
    // public void ChangeMusicVolume(){
    //     // MainMusic.volume = MusicSlider.value;
    // }
    // public void ChangeSoundEffectsVolume(float newVolume){
        // JumpSound.volume = newVolume;
        // GravityShiftSound.volume = newVolume;
        // CheckpointSound.volume = newVolume;
    // }

    // public void UpdateSoundEffectsSlider(){
    //     ChangeSoundEffectsVolume(SoundEffectsSlider.value);
    // }
}
