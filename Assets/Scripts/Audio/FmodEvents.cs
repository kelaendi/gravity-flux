using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FmodEvents : MonoBehaviour
{
    [field: Header("Player Sounds")]
    [field: SerializeField] public EventReference JumpSound { get; private set; }
    [field: SerializeField] public EventReference GravityShiftSound { get; private set; }

    [field: Header("Success Sounds")]
    [field: SerializeField] public EventReference CheckpointSound { get; private set; }
    [field: SerializeField] public EventReference WinSound { get; private set; }

    [field: Header("Death Sounds")]
    [field: SerializeField] public EventReference EnemySound { get; private set; }
    [field: SerializeField] public EventReference RespawnSound { get; private set; }

    public static FmodEvents Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("More than one FmodEvents instance!");
        }
        Instance = this;

    }
}
